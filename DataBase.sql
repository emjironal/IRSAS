USE PROYECTO_ASADA;

/*Creación de las tablas de la base de datos*/

/*Tabla de medidas para los valores de las respuestas*/
CREATE TABLE MEDIDA( 
	ID INTEGER NOT NULL AUTO_INCREMENT, 
    Nombre VARCHAR (50),
    PRIMARY KEY ( ID )
  );
  
/*Tabla de provincias*/
CREATE TABLE PROVINCIA( 
  ID INTEGER NOT NULL, 
  Nombre VARCHAR (10)
  ) ;
ALTER TABLE PROVINCIA ADD CONSTRAINT PROVINCIA_PK PRIMARY KEY ( ID ) ;

/*Tabla de cantones*/
CREATE TABLE CANTON(
    ID           INTEGER NOT NULL ,
    Nombre       VARCHAR (20) ,
    PROVINCIA_ID INTEGER NOT NULL
  ) ;
ALTER TABLE CANTON ADD CONSTRAINT CANTON_PK PRIMARY KEY ( ID, PROVINCIA_ID ) ;

/*Tabla de distritos*/
CREATE TABLE DISTRITO(
    ID                  INTEGER NOT NULL ,
    Nombre              VARCHAR (50) ,
    CANTON_ID           INTEGER NOT NULL ,
    CANTON_PROVINCIA_ID INTEGER NOT NULL,
    Codigo 				INTEGER NOT NULL
  ) ;
ALTER TABLE DISTRITO ADD CONSTRAINT DISTRITO_PK PRIMARY KEY ( ID, CANTON_ID, CANTON_PROVINCIA_ID ) ;

/*Tabla de asadas*/
CREATE TABLE ASADA(
    ID                       INTEGER NOT NULL ,
    Nombre                       VARCHAR (150) ,
    DISTRITO_ID                  INTEGER NOT NULL ,
    DISTRITO_CANTON_ID           INTEGER NOT NULL ,
    DISTRITO_CANTON_PROVINCIA_ID INTEGER NOT NULL
  ) ;
ALTER TABLE Asada ADD CONSTRAINT Asada_PK PRIMARY KEY ( ID ) ;


/*Tabla de Componentes*/
CREATE TABLE COMPONENTE( 
	ID INTEGER NOT NULL AUTO_INCREMENT,
    Nombre VARCHAR (50) NOT NULL, 
    Valor REAL(6,3) NOT NULL,
    PRIMARY KEY ( ID )
  ) ;

/*Tabla de Subcomponentes*/
CREATE TABLE SUBCOMPONENTE( 
	ID INTEGER NOT NULL AUTO_INCREMENT, 
    Nombre VARCHAR (50), 
    valor REAL(6,3) NOT NULL,
	Componente_ID INTEGER NOT NULL,
    PRIMARY KEY ( ID )
  ) ;

/*Tabla de Indicadores*/
CREATE TABLE INDICADOR(
    ID            INTEGER NOT NULL AUTO_INCREMENT,
    Nombre        VARCHAR (50) ,
    Subcomponente_ID INTEGER NOT NULL ,
    Medida_ID     INTEGER NOT NULL,
    Detalle 	  VARCHAR(200) NOT NULL,
    valor			 REAL(6,3) NOT NULL,
    PRIMARY KEY ( ID )
  ) ;

/*Tabla de Respuestas*/
CREATE TABLE INDICADORXASADA(
    ID               INTEGER NOT NULL AUTO_INCREMENT,
    Año              INTEGER NOT NULL ,
    Indicador_ID INTEGER NOT NULL ,
    Asada_ID     INTEGER NOT NULL,
    valor			 REAL(6,3) NOT NULL,
    texto			 VARCHAR(20) NOT NULL,
    PRIMARY KEY ( ID )
  );

/*Tabla de Usuarios*/
CREATE TABLE USUARIO(
	nombre varchar(100),
    usuario varchar(50),
    contrasenna varchar(50),
    tipo integer(1),    
    token varchar(20)
);
ALTER TABLE Usuario ADD CONSTRAINT Usuario_PK PRIMARY KEY ( Usuario ) ;

/*Tabla de Nominales*/
CREATE TABLE NOMINAL(
	ID INTEGER NOT NULL AUTO_INCREMENT,
    Comparativa INTEGER NULL,
    Pendiente REAL(6,3)  NULL,
    Ordenada REAL(6,3)  NULL,
    ValMin INTEGER  NULL,
    ValMax INTEGER  NULL,
    Indicador_ID INTEGER NULL,
    PRIMARY KEY ( ID )
);


/*_______________REFERENCIAS FOREIGN KEYS_______________*/


/*REFERENCIA: ASADA-DISTRITO*/
ALTER TABLE Asada ADD CONSTRAINT Asada_DISTRITO_FK FOREIGN KEY ( DISTRITO_ID, DISTRITO_CANTON_ID, DISTRITO_CANTON_PROVINCIA_ID ) REFERENCES DISTRITO ( ID, CANTON_ID, CANTON_PROVINCIA_ID ) ON
DELETE CASCADE;

/*REFERENCIA: CANTON-PROVINCIA*/
ALTER TABLE CANTON ADD CONSTRAINT CANTON_PROVINCIA_FK FOREIGN KEY ( PROVINCIA_ID ) REFERENCES PROVINCIA ( ID ) ON
DELETE CASCADE ;

/*REFERENCIA: DISTRITO-CANTON*/
ALTER TABLE DISTRITO ADD CONSTRAINT DISTRITO_CANTON_FK FOREIGN KEY ( CANTON_ID, CANTON_PROVINCIA_ID ) REFERENCES CANTON ( ID, PROVINCIA_ID ) ON
DELETE CASCADE ;

/*REFERENCIA: RESPUESTA-INDICADOR*/
ALTER TABLE IndicadorXAsada ADD CONSTRAINT INDICADOR_ASADA_IND_FK FOREIGN KEY ( Indicador_ID ) REFERENCES Indicador ( ID ) ON
DELETE CASCADE ;

/*REFERENCIA: RESPUESTA-ASADA*/
ALTER TABLE IndicadorXAsada ADD CONSTRAINT INDICADOR_ASADA_ASD_FK FOREIGN KEY ( Asada_ID ) REFERENCES Asada ( ID ) ON 
DELETE CASCADE;


/*REFERENCIA: INDICADOR-MEDIDA*/
ALTER TABLE Indicador ADD CONSTRAINT Indicador_Medida_FK FOREIGN KEY ( Medida_ID ) REFERENCES Medida ( ID ) ON
DELETE CASCADE ;

/*REFERENCIA: INDICADOR-SUBCOMPONENTE*/
ALTER TABLE Indicador ADD CONSTRAINT Indicador_Subcomponente_FK FOREIGN KEY ( Subcomponente_ID ) REFERENCES Subcomponente ( ID ) ON
DELETE CASCADE ;

/*REFERENCIA: SUBCOMPONENTE-COMPONENTE*/
ALTER TABLE Subcomponente ADD CONSTRAINT Subcomponente_Componente_FK FOREIGN KEY ( Componente_ID ) REFERENCES Componente ( ID ) ON
DELETE CASCADE ;

/*REFERENCIA: NOMINAL-INDICADOR*/
ALTER TABLE NOMINAL ADD CONSTRAINT NOMINAL_INDICADOR_FK FOREIGN KEY ( Indicador_ID ) REFERENCES indicador ( ID ) ON
DELETE CASCADE ;
